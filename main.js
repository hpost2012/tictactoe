/* jshint esversion:6 */
let currentPlayer = 'X';
let nextPlayer = 'O';
let playerXSelections = [];
let playerOSelections = [];

const winningCombinations = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9],
    [1, 4, 7],
    [2, 5, 8],
    [3, 6, 9],
    [1, 5, 9],
    [3, 5, 7]
];

const handleClick = function(event) {
    const cell = event.target;
    cell.innerHTML = currentPlayer;
    if (currentPlayer === 'X' ) {
        playerSelections = playerXSelections;
        nextPlayer = 'O';
    } else {
        playerSelections = playerOSelections;
        nextPlayer = 'X';
    }
    playerSelections.push(Number(cell.id));
    if (checkWinner(playerSelections)) {
        alert('Player ' + currentPlayer + ' wins!');
        if (currentPlayer==='X'){
            let wins = document.getElementById("xNum").innerText;
            wins = parseInt(wins);
            wins++;
            document.getElementById("xNum").innerText = wins;
        }else {
            let wins = document.getElementById("oNum").innerText;
            wins = parseInt(wins);
            wins++;
            document.getElementById("oNum").innerText = wins;
        }
        resetGame();
      }
      if (checkDraw()) {
        alert('Draw!');
        let wins = document.getElementById("drawNum").innerText;
            wins = parseInt(wins);
            wins++;
            document.getElementById("drawNum").innerText = wins;
        resetGame();
      }
    // Swap players
    currentPlayer = nextPlayer;
}
const cells = document.querySelectorAll('td');
for (let i = 0; i < cells.length; i++) {
    cells[i].addEventListener('click', handleClick);
}
function checkWinner(playerSelctions) {
    // Check if player has all values of each combination
    for (let winCobination = 0; winCobination <= winningCombinations.length-1; winCobination++){
        matches = 0;
        for(let cell=0; cell<=winningCombinations[winCobination].length-1; cell++){
            if (playerSelctions.includes(winningCombinations[winCobination][cell])) {
                matches++;
            }else break;
            if (matches===3) return true;
        }

    }
        
        
    // if we made it through each combo without returning true,
    // then there were no matches and player did not win
    return false;
}
function checkDraw() {
    return (playerOSelections.length + playerXSelections.length) >= cells.length;
}
function resetGame() {
    playerXSelections = new Array();
    playerOSelections = new Array();
    for (let i = 0; i < cells.length; i++) {
        cells[i].innerHTML = '';
    }
}